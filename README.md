High level test cases: https://docs.google.com/document/d/1qq2HPNTUkNW3uElitbftv0f9mZ5TETjUPkZwP18-xOg/edit?usp=sharing

Test cases exported from TestRail: https://docs.google.com/spreadsheets/d/1C9mXd6yoFuUr7Ql7KVueqQhb0be7TtSoUnPpBaI8meo/edit#gid=1198648115

Project in JIRA: https://tatjanakrivobokova.atlassian.net/secure/RapidBoard.jspa?rapidView=2&projectKey=CV&view=planning.nodetail&issueLimit=100

SWOT risk analysis: https://docs.google.com/document/d/1lmizi2IYUizbfrCV_W9kHXzcfdt_znlQ6QQAMj7xihA/edit
